﻿using System;
using System.IO;
using System.Linq;

namespace SampleApp1.Models
{
    public sealed class Customer
    {
        public Customer(string name, string furigana, Gender gender, string phoneNumber, DateTime birthDate)
        {
            Name = name;
            Furigana = furigana;
            Gender = gender;
            PhoneNumber = phoneNumber;
            BirthDate = birthDate;
        }

        public string Name { get; }
        public string Furigana { get; }
        public Gender Gender { get; }
        public string PhoneNumber { get; }
        public DateTime BirthDate { get; }

        public static Customer[] LoadAll()
        {
            return File.ReadAllLines(@"Data\customers.csv")
                .Select(Parse)
                .ToArray();
        }

        public static Customer Parse(string line)
        {
            var cells = line.Split(',');
            return new Customer(cells[1], cells[2], ParseGender(cells[3]), cells[4], ParseBirthDate(cells[5]));
        }

        private static Gender ParseGender(string s)
        {
            switch (s)
            {
                case "男": return Gender.Male;
                case "女": return Gender.Female;
                default: return Gender.NotKnown;
            }
        }

        private static DateTime ParseBirthDate(string s)
        {
            if (DateTime.TryParse(s, out var result)) return result;

            return DateTime.MinValue;
        }
    }
}